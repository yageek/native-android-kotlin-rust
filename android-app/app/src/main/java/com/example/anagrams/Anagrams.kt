package com.example.anagrams

// For holding contents of UI element, to be rendered via RecyclerView
data class Anagrams(val phrase: String)
