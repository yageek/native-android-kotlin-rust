package com.example.anagrams

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.fragment_main.*

// See also: layout/fragment_main.xml

class MainFragment: Fragment() {

    private var query: String = ""

    private var mAnagrams: Resultset = listOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        retainInstance = true
        if (BuildConfig.DEBUG) {
            Log.d("anagrams", "MainFragment.onCreate() query=$query")
        }
    }

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? =
        inflater.inflate(R.layout.fragment_main, container, false)

    // Populates View after Layout has been inflated but occurs before
    // any saved state has been restored into the View.
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (BuildConfig.DEBUG) {
            Log.d("anagrams", "MainFragment onViewCreated() query=$query")
        }
        // Look-up custom ViewModel, and connect to its data:
        val model: ResultsViewModel =
            ViewModelProvider(requireActivity()).get(ResultsViewModel::class.java)

        mAnagrams = model.computeAnagrams(query)
        if (BuildConfig.DEBUG) {
            Log.d("anagrams", " onViewCreated() anagrams size=${mAnagrams.size}")
        }
        if (mAnagrams.size == 0) {
            mAnagrams = listOf("( No matches )")
        }
        // RecyclerView node gets initialized here
        list_recycler_view.apply {
            // set a LinearLayoutManager to handle Android RecyclerView behaviour
            layoutManager = LinearLayoutManager(activity)
            // set the custom adapter to the RecyclerView
            adapter = ListAdapter(mAnagrams)
            Log.d("anagrams", "MainFragment.onViewCreated finished")
        }
    }

    companion object {
        // Constructor: supply `query` String of the user's SEARCH intent.
        fun newInstance(q: String?): MainFragment = MainFragment().apply {
            query = q ?: ""
        }
    }
}
