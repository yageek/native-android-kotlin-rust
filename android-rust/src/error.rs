use std::convert::From;

use crate::log::log_error;

// Coalesce errors from other subsystems
// for keeping top-level code simple via Rust's `?` operator.
#[derive(Debug)]
//#[must_use]
pub enum ErrorKind {
    Anagram(anagram_phrases::error::ErrorKind),
    Jni(String),
    Os(std::io::ErrorKind),
}

impl From<anagram_phrases::error::ErrorKind> for ErrorKind {
    fn from(err: anagram_phrases::error::ErrorKind) -> ErrorKind {
        log_error(format!("Anagrams error={:?}", err));
        ErrorKind::Anagram(err)
    }
}

impl From<jni::errors::Error> for ErrorKind {
    fn from(err: jni::errors::Error) -> ErrorKind {
        log_error(format!("JNI error={:?}", err));
        ErrorKind::Jni(err.description().to_string())
    }
}

impl From<jni::errors::ErrorKind> for ErrorKind {
    fn from(err: jni::errors::ErrorKind) -> ErrorKind {
        log_error(format!("JNI error={:?}", err));
        ErrorKind::Jni(err.description().to_string())
    }
}

impl From<std::io::Error> for ErrorKind {
    fn from(err: std::io::Error) -> ErrorKind {
        log_error(format!("OS io error={:?}", err));
        ErrorKind::Os(err.kind())
    }
}

impl From<std::io::ErrorKind> for ErrorKind {
    fn from(err: std::io::ErrorKind) -> ErrorKind {
        log_error(format!("OS io error={:?}", err));
        ErrorKind::Os(err)
    }
}

